<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shop de vaisseaux</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <header>
        <div class="header">
            <img src="menuburger.png">
            <img src="logoheader.png">
            <img src="logocompte.png">
        </div>
        <h1>La <span>Galaxie</span> à<br> portée de main</h1>
        <div class="headerbloc1">
            <div class="vaisseau1">
                <img src="vaisseau1.png">
                <h2>Découvrez notre<br> nouveau vaisseau</h2>
            </div>
            <h2>La technologie poussée<br>à son paroxysme</h2>
        </div>
    </header>
    <div class="bloc1">
        <h3 class="h3titre1">L’exploration sans limites</h3>
        <section class="slider-wrapper">
            <img src="flechegauche.png" class="slide-arrow" id="slide-arrow-prev">
            &#8249;
            </img>
            <img src="flechedroite.png" class="slide-arrow" id="slide-arrow-next">
            &#8250;
            </img>
            <ul class="slides-container" id="slides-container">
                <li class="slide1"></li>
                <li class="slide1"></li>
                <li class="slide1"></li>
                <li class="slide1"></li>
                <li class="slide1"></li>
                <li class="slide1"></li>
                <li class="slide1"></li>
                <li class="slide1"></li>
                <li class="slide1"></li>
                <li class="slide1"></li>
            </ul>
        </section>
        <h3 class="h3titre2">La Science-Fiction devenu Réalité</h3>
    </div>


    <div class="ensemble-fleche">
        <div class="slide">
            <div class="card cardX"></div>
            <div class="card card1"></div>
            <div class="card card2"></div>
            <div class="card card3"></div>
            <div class="card card4"></div>
            <div class="card card5"></div>
            <div class="card card6"></div>
            <div class="card card7"></div>
            <div class="card card8"></div>
            <div class="card card9"></div>
            <div class="card cardX"></div>
        </div>
        <div class="fleche2emecarousel">
            <img src="carouselgauche2.png" class="img-svg1">
            <img src="carouseldroite2.png" class="img-svg2">
        </div>
    </div>








































    <footer>

    </footer>
    <script src="script.js"></script>
</body>

</html>